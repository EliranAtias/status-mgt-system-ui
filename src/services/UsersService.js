export default {
    getUsers: ()=> {
        return fetch('http://localhost:8080/users/')
          .then(response => response.json())
          .then(data => {
             return data;
          }).catch(e => {
            return e;
          });
    },
    updateUserStatus: (userId, statusDetails) =>{
    return fetch('http://localhost:8080/users/' + userId,{
                method: 'post',
                 headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                 },
                 body: JSON.stringify(statusDetails)
               })
              .then(response => response.json())
              .then(data => {
                 return data;
              }).catch(e => {
                return e;
              });
    }

}